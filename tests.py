from sql_api import StudentDB
# import pytest


def test_default_teacher_bychkov():
    """
    Check Bychkov in DB
    """
    database = StudentDB()
    teacher_id = database.get_teacher_id('Bychkov')
    assert_text_error = "There is not such teacher"
    assert teacher_id == 0, assert_text_error


def test_default_teacher_leikin():
    """
    Check Leikin in DB
    """
    database = StudentDB()
    teacher_id = database.get_teacher_id('Leikin')
    assert_text_error = "There is not such teacher"
    assert teacher_id == 2, assert_text_error


def test_default_group_19_se_1():
    """
    Check 19-SE-1 in DB
    """
    database = StudentDB()
    teacher_id = database.get_group_id('19SE-1')
    assert_text_error = "There is not such group"
    assert teacher_id == 0, assert_text_error


def test_insert_student():
    """
    Check insert student in database
    """
    database = StudentDB()
    student_info = ('Islam', 2, '19SE-1', 'Bychkov')
    database.insert_student(student_info)
    student_id = database.get_student_id('Islam')
    assert_text_error = "There is not such student"
    assert student_id == 1, assert_text_error


def test_insert_teacher():
    """
    Check insert teacher in database
    """
    database = StudentDB()
    teacher_info = ('Efemenko', 'DevOps')
    database.insert_teacher(teacher_info)
    student_id = database.get_teacher_id('Efemenko')
    assert_text_error = "There is not such teacher"
    assert student_id == 3, assert_text_error
