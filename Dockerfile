FROM ubuntu:latest
RUN apt update
RUN apt install -y git
RUN apt install -y python3-pip
RUN git clone https://gitlab.com/murzinov01/DB_tasks.git
RUN cd DB_tasks && cat bot.py
CMD cd DB_tasks && pip3 install -r requirements.txt && python3 bot.py
